const siblingPosition = (element) => {
    var i = 1;
    while (element = element.previousSibling) {
        if (element.nodeType == 1) i += 1;
    }

    return i;
};

const getQuerySelector = (element) => {
    if (element.id) {
        return `#${element.id}`;
    }
    else if (element.nodeName == "BODY") {
        return 'body';
    }
    else {
        let position = siblingPosition(element);
        return `${getQuerySelector(element.parentNode)}>:nth-child(${position})`;
    }
};
const messageReceiver = (response) => {
    let {
        action,
        payload
    } = response;
    switch (action) {
        case 'clickEventTriggered':{
            console.log(`clickEventTriggered ${payload.status} => ${payload.selector}`);
            return true;
        }
    }
};
const messageSender = (message, receiver) => {
    chrome.runtime.sendMessage(message, receiver);
};

document.addEventListener('click', (event) => {
    messageSender({
        action: 'clickEventTriggered',
        payload: {
            domain: window.location.origin,
            pathname: window.location.pathname,
            selector: getQuerySelector(event.target)
        }
    },messageReceiver);
}, true);