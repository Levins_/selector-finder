const elementSelectorNotificationId = '';

const messageListener = ( request, sender, sendResponse ) => {
    let { action, payload } = request;
    switch (action) {
        case 'clickEventTriggered':
            chrome.notifications.create(elementSelectorNotificationId,{
                title: `Click event tracked on ${payload.domain}`,
                message: `Page => ${payload.pathname} ,
Selector : ${payload.selector}`,
                iconUrl: '/notification.png',
                type: 'basic'
            });
            sendResponse({
                action : 'clickEventTriggered',
                payload : {
                    status : 'done',
                    selector : payload.selector
                }
            });
            return true;
    }
    return true;
};
chrome.runtime.onMessage.addListener(messageListener);