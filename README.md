# Click event query selector Tracker
This chrome extension will help you to track the click event on the site. It will notify that event triggered details through the notification. It will contain the domain of that site , path of that page and selector of the element which gets clicked.

# Output's:-
![Notification](./assets/1.png) 
![After expanding the notification](./assets/2.png) 
![Site's console](./assets/3.png)

# Installation
Clone Repo
```
git clone https://gitlab.com/Levins_/selector-finder.git
```

Go to `selector-finder` directory and run
```
npm install
```

Now build the extension using
```
npm run start
```

Refer the below link to install the chrome extension on your browser 
https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked